#include<iostream>
#include<map>
#include<sstream>
#include<fstream>
#include<string.h>

using namespace std;

enum type
{
SEM,LPAR,RPAR,LB,RB,ENDL,SPACE, EQUAL
};
map<char, type> Map;
void printEnum(type);
bool FindOparators(char);
bool FindNumber(string);
bool FindKeyword(string);
void lex(string);

int main()
{
		
	Map.insert(pair<char, type>(';', SEM));
	Map.insert(pair<char, type>('(', LPAR));
	Map.insert(pair<char, type>(')', RPAR));
	Map.insert(pair<char, type>('{', LB));
	Map.insert(pair<char, type>('}', RB));
	Map.insert(pair<char, type>('\n', ENDL));
	Map.insert(pair<char, type>(' ', SPACE));
	Map.insert(pair<char, type>('=', EQUAL));
	
	char fileNam[20];
	cout<<"ENTER FILE NAME = ";
	cin>>fileNam;
	ifstream inputFile(fileNam);
	if(!inputFile.is_open()){
		cout<<"not opening the file\n";
		exit(0);
	}
	stringstream buffer;
	buffer<<inputFile.rdbuf();
	string input=buffer.str();
	lex(input);
	inputFile.close();
	return 0;
}
	
	
	void lex(string input) {
	string operators,line;
	int j = -1;
	int len = input.length();
	for (int i = 0;i <len;i++) {
		if (FindOparators(input[i])) {
			operators = input.substr(i, 1);
			line = input.substr(j + 1, i - j - 1);
			j = i;
			if (!line.empty()) {
				if (FindKeyword(line)) {
					cout << "KEYWORD:" << line << endl;
				}
				else if (FindNumber(line)) {
					cout << "NUMBER:" << line << endl;
				}
				else {
					cout << "ID:" << line << endl;
				}
			}
			printEnum(Map[input[i]]);
		}
	}
}

bool FindOparators(char ch) {
	if (ch == ' ' || ch == '(' || ch == ')' || ch == ',' || ch == '\n' || ch == ';'  || ch == '{' || ch == '}' || ch == '='|| ch == '+' || ch == '*' || ch == '/' || ch == '%') 
		return true;
	else
	return false;
}

bool FindKeyword(string s) {
	if ( s == "int" || s=="for" || s=="else" || s=="float" || s=="double" || s == "return" )
		 return true;
	else
	return false;
}

bool FindNumber(string s) {
	for (int i = 0; i<s.length();) {
		if (s[i] <= '9' && s[i] >= '0' || s[i] == '.') i++;
		else return false;
	}
	return true;
}

void printEnum(type p) {
	if (p == SEM) cout << "SEM"<< ":"<<";"<< endl;
	if (p == LPAR) cout << "LPAR"<< ":"<<"("<< endl;
	if (p == RPAR) cout << "RPAR"<< ":"<<")"<< endl;
	if (p == LB) cout << "LB"<< ":"<<"{"<< endl;
	if (p == RB) cout << "RB"<< ":"<<"}"<< endl;
	if (p == ENDL) cout << "ENDL"<< ":"<<endl;
	if (p == SPACE) cout << "space"<< ":"<< endl;
	if (p == EQUAL) cout << "equal"<< ":"<<"="<< endl;
}
	
	
	


